<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <h2>Sign Up Form</h2>
    <form action="/kirim" method="POST">
        @csrf
         <label>Nama Depan : </label> <br>
        <input type="text" name="fname"> <br><br>
        <label>Nama Belakang :</label>  <br>
        <input type="text" name="lname"> <br><br>
        <label>Gender :</label>  <br>
        <input type="radio" name="gender" > Pria  <br>
        <input type="radio" name="gender" > Wanita  <br>
        <input type="radio" name="gender" > Other <br><br>
        <label>Kewarganegaraan :</label> <br>
        <select name="wn">
            <option value="1"> Warga Negara Indonesia </option>
            <option value="1"> Warga Negara Asing </option>
        </select> <br><br>
        <label>Bahasa Yang Digunakan :</label> <br>
        <input type="checkbox"> Bahasa Indonesia  <br>
        <input type="checkbox"> Bahasa Inggris <br>
        <input type="checkbox"> Bahasa Sunda <br>
        <input type="checkbox"> Yang Lainnya <br><br>
        <label>Bio :</label> <br>
        <textarea name="bio" cols="30" rows="10"></textarea> <br>
        <input type="submit" value=kirim>
    </form>
</body>
</html>